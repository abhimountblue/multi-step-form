const toggle = document.getElementById('toggle_switch')
const twoMonthsFree = document.querySelectorAll('.free-with-yearly')
const arcadePrice = document.getElementById('arcade-price')
const advancedPrice = document.getElementById('advanced-price')
const proPrice = document.getElementById('pro-price')
const firstButton = document.getElementById('step-one-button')
const userName = document.getElementById('name')
const emaiId = document.getElementById('email-id')
const mobileNumber = document.getElementById('mobile-number')
const stepOneSidebar = document.getElementById('step-one-sidebar')
const stepTwoSidebar = document.getElementById('step-two-sidebar')
const errorFeild = document.getElementsByClassName('error-field')
const persionInfo = document.getElementById('personal-info')
const selectPlan = document.getElementById('select-plan')
const backToFirstStep = document.getElementById('step-one-back')
const cards = document.querySelectorAll('.card')
const secondButton = document.getElementById('step-two-button')
const addOns = document.getElementById('add-on')
const backToSecondStep = document.getElementById('step-two-back')
const stepThreeSidebar = document.getElementById('step-three-sidebar')
const onlineServiceAddOn = document.getElementById('online-service')
const storageServiceAddOn = document.getElementById('storage-service')
const customServiceAddOn = document.getElementById('custom-service')
const thirdButton = document.getElementById('step-three-button')
const summary = document.getElementById('summary')
const backToThirdStep = document.getElementById('step-third-back')
const stepFourSidebar = document.getElementById('step-four-sidebar')
const addOnsPrice = document.getElementsByClassName('addons-price')
const addonSummary = document.getElementById('addon-summary')
const priceOfSelectedPlan = document.getElementById('plan-price-summary')
const nameOfSelectedPlan = document.getElementById('plan-name-summary')
const changePlan = document.getElementById('change-plan')
const inputAddOns = document.getElementsByClassName('input-add-ons')
const durationInSummary = document.getElementById('duration-type-summary')
const totalPriceSummary = document.getElementById('total-price-summary')
const confirmButton = document.getElementById('confirm-button')
const thankYou = document.getElementById('thank-you')
const planSelectError = document.getElementById('error-select-plan')
const getAllAddons = document.querySelectorAll('.addons')
const monthlyToggle = document.getElementById('monthly-toggle')
const yearlyToggle = document.getElementById('yearly-toggle')

let addOnsObject = {}
let planDuration = 'monthly'
let selectedPlan = []

toggle.addEventListener('click', () => {
    if (toggle.value === 'on') {
        yearlyToggle.style.color = '#042451'
        monthlyToggle.style.color = 'silver'
        planDuration = 'yearly'
        arcadePrice.innerText = '$90/yr'
        advancedPrice.innerText = '$120/yr'
        proPrice.innerText = '$150/yr'
        for (let index=0;index<twoMonthsFree.length;index++){
            const element = twoMonthsFree[index]
            element.style.display = 'inline'
        }
        toggle.value = 'off'
    } else {
        planDuration = 'monthly'
        yearlyToggle.style.color = 'silver'
        monthlyToggle.style.color = '#042451'
        arcadePrice.innerText = '$9/mo'
        advancedPrice.innerText = '$12/mo'
        proPrice.innerText = '$15/mo'
        for (let index=0;index<twoMonthsFree.length;index++){
            const element = twoMonthsFree[index]
            element.style.display = 'none'
        }
        toggle.value = 'on'
    }
})

firstButton.addEventListener('click', () => {
    errorFeild[0].style.display = 'none'
    errorFeild[1].style.display = 'none'
    errorFeild[2].style.display = 'none'
    userName.style.border = '1px solid silver'
    emaiId.style.border = '1px solid silver'
    mobileNumber.style.border = '1px solid silver'

    const nameFormat = /^[a-zA-Z ]+$/
    if (userName.value === '') {
        userName.style.border = '2px solid red'
        errorFeild[0].style.display = 'inline'
        return
    } else if (userName.value.length < 3) {
        userName.style.border = '2px solid red'
        errorFeild[0].innerText = 'Name  should be more than 2 character'
        errorFeild[0].style.display = 'inline'
        return
    } else if (nameFormat.test(userName.value) === false) {
        userName.style.border = '2px solid red'
        errorFeild[0].innerText = 'Name should be only alphabetic character'
        errorFeild[0].style.display = 'inline'
        return
    }

    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (emaiId.value.length === 0) {
        emaiId.style.border = '2px solid red'
        errorFeild[1].style.display = 'inline'
        return
    } else if (mailFormat.test(emaiId.value) === false) {
        emaiId.style.border = '2px solid red'
        errorFeild[1].innerText = 'Please enter a valid email address.'
        errorFeild[1].style.display = 'inline'
        return
    }

    const mobileFormat = /^(\+|00)[1-9][0-9 \-\(\)\.]{7,32}$/
    if (mobileNumber.value.length === 0) {
        mobileNumber.style.border = '2px solid red'
        errorFeild[2].style.display = 'inline'
        return
    }
    else if (mobileFormat.test(mobileNumber.value) === false) {
        mobileNumber.style.border = '2px solid red'
        errorFeild[2].innerText = 'Please enter a valid Number.'
        errorFeild[2].style.display = 'inline'
        return
    }
    const userDetail = {
        name: userName.value,
        email: emaiId.value,
        mobile: mobileNumber.value
    }
    persionInfo.style.display = 'none'
    selectPlan.style.display = 'flex'
    stepTwoSidebar.style.background = '#BEE2FD'
    stepTwoSidebar.style.color = '#06294F'
    stepOneSidebar.style.background = 'none'
    stepOneSidebar.style.color = '#F5F5F5'
})

backToFirstStep.addEventListener('click', () => {
    persionInfo.style.display = 'flex'
    selectPlan.style.display = 'none'
    stepTwoSidebar.style.background = 'none'
    stepTwoSidebar.style.color = '#F5F5F5'
    stepOneSidebar.style.background = '#BEE2FD'
    stepOneSidebar.style.color = '#06294F'
})


secondButton.addEventListener('click', () => {
    if (selectPlan[0] === undefined) {
        planSelectError.style.display = 'inline'
        return
    }
    if (planDuration === 'monthly') {
        addOnsPrice[0].innerText = '+$1/mo'
        addOnsPrice[1].innerText = '+$2/mo'
        addOnsPrice[2].innerText = '+$2/mo'
    } else {
        addOnsPrice[0].innerText = '+$10/yr'
        addOnsPrice[1].innerText = '+$20/yr'
        addOnsPrice[2].innerText = '+$20/yr'
    }
    selectPlan.style.display = 'none'
    addOns.style.display = 'flex'
    stepThreeSidebar.style.background = '#BEE2FD'
    stepThreeSidebar.style.color = '#06294F'
    stepTwoSidebar.style.background = 'none'
    stepTwoSidebar.style.color = '#F5F5F5'

})

for (let index = 0; index < cards.length; index++) {
    const element = cards[index]
    element.addEventListener('click', (event) => {
        event.stopPropagation()
        if (planSelectError.style.display === 'inline') {
            planSelectError.style.display = 'none'
        }
        const id = element.getAttribute('id')
        setBackGroundOfCard(id)
    })
}


function setBackGroundOfCard(id) {
    const element = document.getElementById(id)
    if (selectPlan[0] === id) {
        selectPlan[1] = element.children[1].children[1].innerText
        return
    } else {
        if (selectPlan[0] === undefined) {
            selectPlan[0] = id
            element.style.border = '2px solid #756DBB'
            element.style.background = '#F8F9FF'
            selectPlan[1] = element.children[1].children[1].innerText
        } else {
            const previosElementId = selectPlan[0]
            const previosElement = document.getElementById(previosElementId)
            selectPlan[0] = id
            selectPlan[1] = element.children[1].children[1].innerText
            element.style.border = '2px solid #756DBB'
            element.style.background = '#F8F9FF'
            previosElement.style.border = '2px solid #b1b1b7'
            previosElement.style.background = 'none'
        }
    }
}

backToSecondStep.addEventListener("click", () => {
    selectPlan.style.display = 'flex'
    addOns.style.display = 'none'
    stepThreeSidebar.style.background = 'none'
    stepThreeSidebar.style.color = '#F5F5F5'
    stepTwoSidebar.style.background = '#BEE2FD'
    stepTwoSidebar.style.color = '#06294F'
})




thirdButton.addEventListener('click', () => {
    const selectedCard = document.getElementById(selectPlan[0])
    const selectedCardPrice = selectedCard.children[1].children[1].innerText
    addOns.style.display = 'none'
    summary.style.display = 'flex'
    stepFourSidebar.style.background = '#BEE2FD'
    stepFourSidebar.style.color = '#06294F'
    stepThreeSidebar.style.background = 'none'
    stepThreeSidebar.style.color = '#F5F5F5'
    priceOfSelectedPlan.innerText = selectedCardPrice
    let finalPriceCount = Number(selectedCardPrice.slice(1).split('/')[0])
    durationInSummary.innerText = `per ${planDuration === 'monthly' ? 'month' : 'year'}`
    for (let index = 0; index < inputAddOns.length; index++) {
        const element = inputAddOns[index]
        if (element.checked === true) {
            const serviceName = element.parentElement.children[1].children[0].innerText
            const servicePrice = element.parentElement.parentElement.children[1].innerText
            const price = servicePrice.slice(2).split('/')[0]
            finalPriceCount = finalPriceCount + Number(price)
            addOnsObject[serviceName] = servicePrice
        }
    }
    nameOfSelectedPlan.innerText = `${selectPlan[0].charAt(0).toUpperCase() + selectPlan[0].slice(1)} (${planDuration.charAt(0).toUpperCase() + planDuration.slice(1)})`
    for (key in addOnsObject) {
        const newDiv = document.createElement('div')
        const addOnName = document.createElement('p')
        const addOnPrice = document.createElement('p')
        addOnName.classList.add('addon-name')
        addOnName.append(key)
        addOnPrice.classList.add('addon-price')
        addOnPrice.append(addOnsObject[key])
        newDiv.append(addOnName)
        newDiv.append(addOnPrice)
        newDiv.classList.add('selected-addons')
        addonSummary.append(newDiv)
    }
    const finalPriceDuration = planDuration === 'monthly' ? 'mo' : 'yr'
    totalPriceSummary.innerText = `$${finalPriceCount}/${finalPriceDuration}`
})

backToThirdStep.addEventListener('click', () => {
    addOns.style.display = 'flex'
    summary.style.display = 'none'
    stepFourSidebar.style.background = 'none'
    stepFourSidebar.style.color = '#F5F5F5'
    stepThreeSidebar.style.background = '#BEE2FD'
    stepThreeSidebar.style.color = '#06294F'
    addOnsObject = {}
    while (addonSummary.firstChild) {
        addonSummary.firstChild.remove()
    }
})

changePlan.addEventListener('click', () => {
    selectPlan.style.display = 'flex'
    stepFourSidebar.style.background = 'none'
    stepFourSidebar.style.color = '#F5F5F5'
    stepTwoSidebar.style.background = '#BEE2FD'
    stepTwoSidebar.style.color = '#06294F'
    summary.style.display = 'none'
    addOnsObject = {}
    while (addonSummary.firstChild) {
        addonSummary.firstChild.remove()
    }
})

confirmButton.addEventListener('click', () => {
    summary.style.display = 'none'
    thankYou.style.display = 'flex'
})


for (let index = 0; index < getAllAddons.length; index++) {
    const element = getAllAddons[index]
    const checkBoxValue = element.querySelector('input[type="checkbox"]')

    element.addEventListener('click', (event) => {
        event.stopPropagation()

        if (checkBoxValue.checked === true) {
            checkBoxValue.checked = false
        } else {
            checkBoxValue.checked = true
        }

        if (checkBoxValue.checked === true) {
            element.style.background = '#F8F9FF'
            element.style.border = '2px solid #756DBB'
        } else {
            element.style.background = 'none'
            element.style.border = '2px solid silver'
        }
    })

    checkBoxValue.addEventListener('click', (event) => {
        event.stopPropagation()

        if (checkBoxValue.checked === true) {
            element.style.background = '#F8F9FF'
            element.style.border = '2px solid #756DBB'
        } else {
            element.style.background = 'none'
            element.style.border = '2px solid silver'
        }
    })
}

